;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid vicinity-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid vicinity))
  (begin
    (define (run-tests)
    
      (test-begin "Vicinity")

      (test-group "System independent tests"
	(define vicinity (make-vicinity "foo/"))

	(test-equal "in-vicinity"
	  "foo/bar.scm"
	  (in-vicinity vicinity "bar.scm"))

	(test-equal "sub-vicinity"
	  "foo/bar/baz.scm"
	  (in-vicinity (sub-vicinity vicinity "bar") "baz.scm"))

	(test-equal "pathname->vicinity"
	  "/foo/baz.scm"
	  (in-vicinity (pathname->vicinity "/foo/bar.scm") "baz.scm"))

	(test-assert "vicinity:suffix?: /"
	  (vicinity:suffix? #\/))

	(test-assert "vicinity-suffix?: \\"
	  (not (vicinity:suffix? #\\))))
      
      (test-end))))
